const keyValue = "eae633dae6fa76a149ad9223c1a2c552";
const tokenValue =
  "ATTA07a64511c9fd8ab2e80abcea1105c3e240d0b1e1cd98104ccf3a0f990634655d0734081C";

export function createNewBoard(boardName) {
  return fetch(
    `https://api.trello.com/1/boards/?name=${boardName}&key=${keyValue}&token=${tokenValue}`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
    }
  )
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.json();
    })
    .then((data) => {
      console.log(data);
    })
    .catch((error) => {
      console.error("There was a problem with the fetch operation:", error);
    });
}

export function fetchBoard() {
  const boardData = [];
  return fetch(
    `https://api.trello.com/1/members/me/boards?key=${keyValue}&token=${tokenValue}`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  )
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((error) => {
      console.error("There was a problem with the fetch operation:", error);
    });
}

function createfetch(path, tempMethod, body) {
  return fetch(path, {
    method: tempMethod,
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  });
}

export function getLists(boardId) {
  return createfetch(
    `https://api.trello.com/1/boards/${boardId}/lists?key=${keyValue}&token=${tokenValue}`,
    "GET"
  )
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.json();
    })
    .catch((error) => {
      console.log("Error getting list:", error.message);
    });
}

export function getCards(listId) {
  return fetch(
    `https://api.trello.com/1/lists/${listId}/cards?key=${keyValue}&token=${tokenValue}`
  )
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.json();
    })
    .catch((error) => {
      console.log("Error getting card info:", error.message);
    });
}

export function createCards(cardName, listId) {
  return createfetch(
    `https://api.trello.com/1/cards?key=${keyValue}&token=${tokenValue}`,
    "POST",
    { name: cardName, idList: listId }
  );
}

export function createLists(listName, boardId) {
  return createfetch(
    `https://api.trello.com/1/lists?key=${keyValue}&token=${tokenValue}`,
    "POST",
    { name: listName, idBoard: boardId }
  );
}

export function deleteList(listId) {
  return createfetch(
    `https://api.trello.com/1/lists/${listId}/closed?key=${keyValue}&token=${tokenValue}`,
    "PUT",
    {
      value: true,
    }
  );
}

export function deleteCard(cardid) {
  return createfetch(
    `https://api.trello.com/1/cards/${cardid}?key=${keyValue}&token=${tokenValue}`,
    "DELETE"
  );
}
