import { createNewBoard, fetchBoard } from "./script.js";

document.addEventListener("DOMContentLoaded", () => {
  const newBoardButton = document.getElementById("new-board");
  const modal = document.getElementById("my_modal_1");
  const closeModalButton = document.getElementById("closeModal");
  const modalForm = document.getElementById("modalForm");
  const boardsContainer = document.getElementById("boards-container");

  newBoardButton.addEventListener("click", () => {
    modal.showModal();
  });

  closeModalButton.addEventListener("click", () => {
    modal.close();
  });

  modalForm.addEventListener("submit", (event) => {
    event.preventDefault();
    const boardName = document.getElementById("board-name").value;
    createNewBoard(boardName)
      .then(() => {
        window.location.reload();
      })
      .catch((error) => {
        console.error("Error creating board:", error);
      });
  });

  function displayBoard(boards) {
    boards.forEach((board) => {
      
      const { prefs, name, id } = board;
      const boardElement = document.createElement("button");
      boardElement.className =
        "border-box m-2 w-1/4 rounded-lg text-2xl text-black h-40 flex items-center justify-center";
        
        const path=document.createElement("a");
        path.style.src="./boardPage.html"   
        path.innerText = name;
      
        if (prefs.backgroundImage) {
        boardElement.style.backgroundImage = `url(${prefs.backgroundImage})`;
        boardElement.style.backgroundSize = "cover";
      } else {
        boardElement.style.backgroundColor = prefs.backgroundColor;
      }

      boardElement.addEventListener("click", () => {
        window.location.href = `./boardPage.html?boardId=${id}`;
      });
      boardElement.appendChild(path);  

      boardsContainer.appendChild(boardElement);
    });
  }

  function fetchAndDisplayBoards() {
    fetchBoard()
      .then((boards) => {
        displayBoard(boards);
      })
      .catch((error) => {
        console.error("Error fetching boards:", error);
      });
  }

  fetchAndDisplayBoards();
});
