import {
  getLists,
  getCards,
  createCards,
  createLists,
  deleteList,
  deleteCard,
} from "./script.js"; 

const boardId = getBoardIdFromUrl();
const listBox = document.getElementById("list-box");

function getBoardIdFromUrl() {
  const params = new URLSearchParams(window.location.search);
  return params.get("boardId");
}

function showDialog(message, onConfirm) {
  const dialog = document.getElementById("confirmation-dialog");
  const dialogMessage = document.getElementById("dialog-message");
  const confirmButton = document.getElementById("confirm-button");
  const cancelButton = document.getElementById("cancel-button");

  dialogMessage.innerText = message;
  dialog.classList.remove("hidden");

  const confirmAction = () => {
    onConfirm();
    dialog.classList.add("hidden");
    confirmButton.removeEventListener("click", confirmAction);
    cancelButton.removeEventListener("click", cancelAction);
  };

  const cancelAction = () => {
    dialog.classList.add("hidden");
    confirmButton.removeEventListener("click", confirmAction);
    cancelButton.removeEventListener("click", cancelAction);
  };

  confirmButton.addEventListener("click", confirmAction);
  cancelButton.addEventListener("click", cancelAction);
}

function createCard(cardName, listId) {
  return createCards(cardName, listId)
    .then(() => {
      form.remove();
      newCardButton.style.display = "inline-block";
    })
    .catch((error) => {
      console.error("Error creating card:", error);
    });
}

function fetchLists() {
  return getLists(boardId)
    .then((lists) => {
      listBox.innerHTML = "";

      lists.forEach((list) => {
        const listElement = document.createElement("div");
        listElement.className =
          "flex-shrink-0 w-72 border flex-col text-center h-full rounded-lg text-xl bg-gray-200 p-4";

        const listNameContainer = document.createElement("div");
        listNameContainer.className =
          "flex justify-between items-center w-full mb-2 h-auto rounded-lg text-xl bg-sky-950 text-white text-center p-2";

        const listName = document.createElement("div");
        listName.innerText = list.name;

        const deleteListButton = document.createElement("button");
        deleteListButton.className = "bg-transparent p-1 m-1";
        deleteListButton.innerHTML = `<i class="fa-solid fa-ellipsis"></i>`;
        deleteListButton.addEventListener("click", () => {
          showDialog(`Do you want to delete the list "${list.name}"?`, () => {
            deleteList(list.id)
              .then(() => window.location.reload())
              .catch((error) => console.error("Error deleting list:", error));
          });
        });

        listNameContainer.appendChild(listName);
        listNameContainer.appendChild(deleteListButton);
        listElement.appendChild(listNameContainer);

        const listId = list.id;

        getCards(listId)
          .then((cards) => {
            cards.forEach((card) => {
              const cardElement = document.createElement("div");
              cardElement.className =
                "border-box my-4 p-4 w-full h-12 rounded-lg text-xl bg-white text-center flex justify-between items-center";
              cardElement.innerText = card.name;

              const deleteCardButton = document.createElement("button");
              deleteCardButton.className = "bg-transparent p-1 m-1";
              deleteCardButton.innerHTML = `<i class="fa-solid fa-pencil"></i>`;
              deleteCardButton.addEventListener("click", () => {
                showDialog(
                  `Do you want to delete the card "${card.name}"?`,
                  () => {
                    deleteCard(card.id)
                      .then(() => window.location.reload())
                      .catch((error) =>
                        console.error("Error deleting card:", error)
                      );
                  }
                );
              });

              cardElement.appendChild(deleteCardButton);
              listElement.appendChild(cardElement);
            });
          })
          .then(() => {
            const newCardButton = document.createElement("button");
            newCardButton.className =
              "border-box mt-2 w-full rounded-lg text-xl h-12 bg-sky-950 text-white text-center";
            newCardButton.innerText = " + Add a Card";
            listElement.appendChild(newCardButton);

            newCardButton.addEventListener("click", () => {
              newCardButton.style.display = "none";
              const existingForm = listElement.querySelector(".new-card-form");
              if (existingForm) {
                existingForm.remove();
              }

              const form = document.createElement("form");
              form.className =
                "new-card-form mt-2 border rounded bg-gray-100 p-2";

              const input = document.createElement("input");
              input.type = "text";
              input.className = "border mt-2 w-full p-2";
              input.placeholder = "Enter card details";

              const submitButton = document.createElement("button");
              submitButton.type = "submit";
              submitButton.className =
                "text-white bg-sky-950 rounded mt-2 px-4 py-2";
              submitButton.innerText = "Submit";

              const closeButton = document.createElement("button");
              closeButton.type = "button";
              closeButton.className =
                "text-white bg-red-500 rounded mt-2 px-4 py-2 ml-2";
              closeButton.innerText = "Close";

              form.appendChild(input);
              form.appendChild(submitButton);
              form.appendChild(closeButton);
              listElement.appendChild(form);

              form.addEventListener("submit", (e) => {
                e.preventDefault();
                const cardName = input.value;

                createCard(cardName, listId).then(() => {
                  form.remove();
                  newCardButton.style.display = "inline-block";
                  window.location.reload();
                });
              });

              closeButton.addEventListener("click", () => {
                form.remove();
                newCardButton.style.display = "inline-block";
              });
            });
          })
          .catch((error) => {
            console.error("Error fetching cards:", error);
          });

        listBox.appendChild(listElement);
      });
    })
    .catch((error) => {
      console.error("Error fetching lists:", error);
    });
}

document.addEventListener("DOMContentLoaded", () => {
  fetchLists().then(() => {
    const createListContainer = document.createElement("div");
    createListContainer.className = "flex-shrink-0 w-72 border-box rounded-lg";

    const listButton = document.createElement("button");
    listButton.className =
      "border-box w-72 h-24 rounded-lg text-xl bg-sky-950 text-white text-center";
    listButton.innerText = " + Add new List";

    createListContainer.appendChild(listButton);
    listBox.appendChild(createListContainer);

    listButton.addEventListener("click", () => {
      listButton.style.display = "none";

      const existingForm = document.querySelector(".new-list-form");
      if (existingForm) {
        existingForm.remove();
      }

      const form = document.createElement("form");
      form.className = "new-list-form border w-72 mt-2 rounded bg-gray-100 p-4";

      const input = document.createElement("input");
      input.type = "text";
      input.className = "border w-full p-2";
      input.placeholder = "Enter List Name";

      const submitButton = document.createElement("button");
      submitButton.type = "submit";
      submitButton.className =
        "text-black mt-2 bg-sky-950 text-white rounded px-4 py-2";
      submitButton.innerText = "Submit";

      const closeButton = document.createElement("button");
      closeButton.type = "button";
      closeButton.className =
        "text-white bg-red-500 rounded mt-2 px-4 py-2 ml-2";
      closeButton.innerText = "Close";

      form.appendChild(input);
      form.appendChild(submitButton);
      form.appendChild(closeButton);
      createListContainer.appendChild(form);

      form.addEventListener("submit", (e) => {
        e.preventDefault();
        const listName = input.value;

        createLists(listName, boardId)
          .then(() => {
            form.remove();
            listButton.style.display = "inline-block";
            window.location.reload();
          })
          .catch((error) => {
            console.error("Error creating list:", error);
          });
      });

      closeButton.addEventListener("click", () => {
        form.remove();
        listButton.style.display = "inline-block";
      });
    });
  });
});
